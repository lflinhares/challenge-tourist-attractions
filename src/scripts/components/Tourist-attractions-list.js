export class TouristAttractionsList {
  constructor() {
    this.selectors();
    this.listeners();
    this.itemsArray = [
      {
        name: "Pão de açúcar",
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        path: "assets/images/pao-de-acucar.png",
        id: 1,
      },
      {
        name: "Cristo Redentor",
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        path: "/assets/images/cristo-redentor.png",
        id: 2,
      },
      {
        name: "Ilha Grande",
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        path: "/assets/images/ilha-grande.png",
        id: 3,
      },
      {
        name: "Centro Histórico de Paraty",
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        path: "/assets/images/paraty.png",
        id: 4,
      },
    ];
    this.renderListItems();

    this.createRemoveButtonEventListener();
    this.uploadedImage = "";
  }

  selectors() {
    this.itemNameInput = document.querySelector("#title-input");
    this.itemDescriptionInput = document.querySelector("#description-input");
    this.touristAttractionsGrid = document.querySelector("#tourist-attractions-grid");
    this.addItemButtonEl = document.querySelector("#submit-button");
    this.imageInput = document.querySelector("#img-input");
    this.imageInputPreview = document.querySelector("#img-input-container");
    this.imageInputPlaceholder = document.querySelector(".img-input-placeholder");
    this.snackBarEl = document.querySelector("#snack-bar");
  }

  listeners() {
    this.addItemButtonEl.addEventListener("click", this.addButtonEventHandler.bind(this));
    this.imageInput.addEventListener("change", this.uploadImageEventHandler.bind(this));
  }

  uploadImage(event) {
    this.uploadedImage = URL.createObjectURL(event.target.files[0]);
  }

  setPreviewImg() {
    this.imageInputPreview.style.background = `url(${this.uploadedImage}) no-repeat  center`;
    this.imageInputPreview.style.backgroundSize = "cover";
    this.imageInputPlaceholder.style.display = "none";
  }

  uploadImageEventHandler(event) {
    this.uploadImage(event);
    this.setPreviewImg();
  }

  addButtonEventHandler(event) {
    event.preventDefault();
    this.addListItemHandler();
    this.renderListItems();
    this.createRemoveButtonEventListener();
  }

  addListItemHandler() {
    if (
      this.itemDescriptionInput.value !== "" &&
      this.itemNameInput.value !== "" &&
      this.uploadedImage !== ""
    ) {
      this.itemsArray.unshift({
        name: this.itemNameInput.value,
        description: this.itemDescriptionInput.value,
        path: this.uploadedImage,
        id: this.setItemsId(),
      });
      this.resetInputs();
    } else {
      this.snackBar();
    }
  }

  setItemsId() {
    if (this.itemsArray.length === 0) {
      return 1;
    }
    const newId = this.itemsArray.sort((current, next) => {
      return next.id - current.id;
    });
    return newId[0].id + 1;
  }

  snackBar() {
    this.snackBarEl.classList.add("snack-bar-active");
    setTimeout(() => {
      this.snackBarEl.classList.remove("snack-bar-active");
    }, 4000);
  }

  renderListItems() {
    this.touristAttractionsGrid.innerHTML = "";
    this.itemsArray.forEach((item) => {
      let itemContainer = document.createElement("div");
      let imgContainer = document.createElement("div");
      let infosContainer = document.createElement("div");
      let itemName = document.createElement("div");
      let itemDescription = document.createElement("div");
      let removeButton = document.createElement("button");

      itemContainer.setAttribute("class", "item-container");
      imgContainer.setAttribute("class", "img-container");
      infosContainer.setAttribute("class", "infos-container");
      itemName.setAttribute("class", "item-name");
      itemDescription.setAttribute("class", "item-description");
      removeButton.setAttribute("class", "remove-button");

      imgContainer.style.background = `url(${item.path}) no-repeat center`;
      imgContainer.style.backgroundSize = "cover";
      itemName.innerText = item.name;
      itemDescription.innerText = item.description;
      removeButton.dataset.id = item.id;
      removeButton.innerText = "X";

      itemContainer.append(imgContainer, infosContainer, removeButton);
      infosContainer.append(itemName, itemDescription);

      this.touristAttractionsGrid.appendChild(itemContainer);
    });
  }

  createRemoveButtonEventListener() {
    const removeItemButtonEl = document.querySelectorAll(".remove-button");
    for (let i = 0; i < removeItemButtonEl.length; i++) {
      removeItemButtonEl[i].addEventListener("click", this.removeItemEventHandler.bind(this));
    }
  }

  removeItemEventHandler(event) {
    this.removeItem(event);
    this.renderListItems();
    this.createRemoveButtonEventListener();
  }

  removeItem(event) {
    this.itemsArray = this.itemsArray.filter(
      (item) => item.id !== parseInt(event.target.dataset.id)
    );
  }

  resetInputs() {
    this.itemDescriptionInput.value = "";
    this.itemNameInput.value = "";
    this.imageInput.value = "";
    this.imageInputPreview.style.background = "none";
    this.imageInputPlaceholder.style.display = "block";
    this.uploadedImage = "";
  }
}
